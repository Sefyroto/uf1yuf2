package juego;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

//TODO poner ficha, comprobar posicion de ficha, comprobar horizontal vertical diagonal para condicion de victoria.

public class Conecta4 {

	static int[][] tablero;
	static Scanner sc = new Scanner(System.in);
	static Random r = new Random();

	public static void main(String[] args) {
		String[] opciones = { "6", "7", "Jugador1", "Jugador2" };
		ArrayList<String> ganadores = new ArrayList<>();
		boolean salir = false;
		while (salir != true) {
			System.out.println("Elige una opcion : ");
			System.out.println("1. Mostrar ayuda");
			System.out.println("2. Opciones");
			System.out.println("3. Jugar");
			System.out.println("4. Ranking");
			System.out.println("0. Salir");
			System.out.println(" ");

			int opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				ayuda();
				break;
			case 2:
				opciones = opciones();
				break;
			case 3:
				ganadores.add(jugar(opciones));
				break;
			case 4:
				ranking(ganadores);
				break;
			case 0:
				salir = true;
				System.out.println("Has salido, espero volver a verte");
				System.out.println(" ");
				break;

			default:
				System.out.println("No existe esa opcion");
				System.out.println(" ");
				break;
			}
		}

	}

	/**
	 * Se puede ver los ultimos ganadores o empates
	 * 
	 * @param ganadores
	 */
	private static void ranking(ArrayList<String> ganadores) {
		System.out.println(ganadores);
	}

	/**
	 * Es el juego en si
	 * 
	 * @param opciones
	 * @return
	 */
	private static String jugar(String[] opciones) {

		int x = Integer.parseInt(opciones[0]);
		int y = Integer.parseInt(opciones[1]);
		String jugador1 = opciones[2];
		String jugador2 = opciones[3];

		tablero = new int[x][y];
		// System.out.println(tablero.length);

		imprimir();
		int turno = 1;
		int columna = 0;
		boolean linea = false;
		int turnos=0;

		boolean finpartida = false;
		while (finpartida != true) {
			columna = pedirficha(turno, jugador1, jugador2, x);
			caidadeficha(columna, turno);
			linea = comprobarlinea(turno);
			finpartida = acabarpartida(linea);
			if (finpartida != true) {
				turno = cambiodeturno(turno);
				turnos++;
			}
		}
		if (empate() == true) {
			System.out.println("No ha ganado nadie");
			return "Empate";
		} else if (turno == 1) {
			System.out.println("Ha ganado el jugador " + jugador1 + " con la ficha numero 1 en "+turnos+" turnos.");
			return "Ha ganado "+jugador1+" en "+ turnos +" turnos ";
		} else {
			System.out.println("Ha ganado el jugador " + jugador2 + " con la ficha numero 2 en "+turnos+" turnos.");
			return "Ha ganado "+jugador2+" en "+ turnos +" turnos ";
		}

	}

	/**
	 * Se comprueba si hay una linea o el tablero esta lleno para acabar la partida
	 * 
	 * @param linea
	 * @return
	 */
	private static boolean acabarpartida(boolean linea) {
		boolean lleno = true;
		if (linea == true) {
			return true;
		} else {
			lleno = empate();

			if (lleno != true) {
				return false;
			} else {
				System.out.println("Empate");
				return true;
			}
		}
	}

	/**
	 * Se comprueba si se a llenado el tablero, si es asi es empate
	 * 
	 * @return
	 */
	private static boolean empate() {
		boolean lleno = true;
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				if (tablero[i][j] == 0) {
					lleno = false;
				}
			}
		}
		return lleno;
	}

	/**
	 * Se comprueba si hay una linea en cualquier direcion
	 * 
	 * @param turno
	 * @return
	 */
	private static boolean comprobarlinea(int turno) {

		if (horizontal(turno) == true || vertical(turno) == true || diagonal(turno) == true) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Comprobacion diagonal
	 * 
	 * @param turno
	 * @return
	 */
	private static boolean diagonal(int turno) {

		boolean correcto = false;

		for (int fila = 0; fila < tablero.length; fila++) {
			if (correcto) {
				break;
			}
			for (int col = 0; col < tablero[0].length; col++) {
				if (correcto)
					break;
				// Comprueba inferiores
				if (fila + 3 < tablero.length) {

					// DERECHA
					if (col + 3 < tablero[0].length) {
						int count = 0;
						for (int i = fila + 1; i < fila + 4; i++) {
							if (tablero[i][col + (i - fila)] == turno && tablero[fila][col] == turno) {
								count++;
							}
						}
						if (count == 3) {
							correcto = true;
							System.out.println("Victoria diagonal");
						}
					}

					// IZQUIERDA
					if (col - 3 >= 0) {
						int count = 0;
						for (int i = fila + 1; i < fila + 4; i++) {
							if (tablero[i][col - (i - fila)] == turno && tablero[fila][col] == turno) {
								count++;
							}
						}
						if (count == 3) {
							correcto = true;
							System.out.println("Victoria diagonal");
						}
					}
				}
			}
		}
		return correcto;
	}

	/**
	 * Comprobacion vertical
	 * 
	 * @param turno
	 * @return
	 */
	private static boolean vertical(int turno) {
		int contador = 0;

		for (int j = 0; j < tablero[0].length; j++) {
			contador = 0;
			for (int i = 0; i < tablero.length; i++) {
				if (tablero[i][j] != turno) {
					contador = 0;
				} else if (tablero[i][j] == turno) {
					contador++;
					// System.out.println(contador + " loquesea vertical ");
					if (contador == 4) {
						System.out.println("Victoria vertical");
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Comprobacion horizontal
	 * 
	 * @param turno
	 * @return
	 */
	private static boolean horizontal(int turno) {
		int contador = 0;
		for (int i = 0; i < tablero.length; i++) {
			contador = 0;
			for (int j = 0; j < tablero[0].length; j++) {
				if (tablero[i][j] != turno) {
					contador = 0;
				} else if (tablero[i][j] == turno) {
					contador++;
					// System.out.println(contador + " loquesea horizontal ");
					if (contador == 4) {
						System.out.println("Victoria horizontal");
						return true;
					}
				}
			}

		}

		return false;
	}

	/**
	 * Se comprueba en que fila de la columna se va a quedar la ficha
	 * 
	 * @param columna
	 * @param turno
	 */
	private static void caidadeficha(int columna, int turno) {

		int fila = 0;

		while (fila < tablero.length - 1 && tablero[fila + 1][columna] == 0) {
			fila++;
		}
		tablero[fila][columna] = turno;
		imprimir();
	}

	/**
	 * Se le pide al jugardor posicion de fila
	 * 
	 * @param turno
	 * @param jugador1
	 * @param jugador2
	 * @param x
	 * @return
	 */
	private static int pedirficha(int turno, String jugador1, String jugador2, int x) {
		int columna = 0;
		boolean comprobacion = false;

		while (comprobacion != true) {
			if (turno==1) {
				if (jugador1.equalsIgnoreCase("CPU")) {
					columna = r.nextInt(x);
				}else {
					System.out.println("En que columna quieres posicionar tu ficha " + jugador1 + "?");
					columna = sc.nextInt();
				}
			}else {
				if (jugador2.equalsIgnoreCase("CPU")) {
					columna = r.nextInt(x);
				}else {
					System.out.println("En que columna quieres posicionar tu ficha " + jugador2 + "?");
					columna = sc.nextInt();
				}
			}
			comprobacion = comprobar(columna);
		}
		System.out.println("Se a insertado la ficha "+ turno +" en la columna " + columna);
		return columna;
	}

	/**
	 * Se comprueba si la ficha puede insertarse en la casilla selecionada
	 * 
	 * @param columna
	 * @return
	 */
	private static boolean comprobar(int columna) {
		if (columna >= tablero[0].length) {
			System.out.println("La ficha no esta en un lugar correcto ");
			return false;
		} else if (columna < 0) {
			System.out.println("La ficha no esta en un lugar correcto ");
			return false;
		} else if (tablero[0][columna] != 0) {
			System.out.println("Ya hay una ficha aqui ");
			return false;
		} else {
			return true;
		}

	}

	/**
	 * Una vez se termina de colocar la ficha y se comprueba si hay linea o no se
	 * cambia de turno
	 * 
	 * @param turno
	 * @return
	 */
	private static int cambiodeturno(int turno) {
		if (turno == 1) {
			turno = 2;
		} else {
			turno = 1;
		}

		return turno;
	}

	/**
	 * Nos permite imprimir el tablero
	 */
	private static void imprimir() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Se puede modificar las opciones desde aqui
	 * 
	 * @return
	 */
	private static String[] opciones() {
		boolean bien = false;
		String x = "";
		String y = "";
		while (bien != true) {
			System.out.println("Inserta el numero de columnas");
			int columna = sc.nextInt();
			x = Integer.toString(columna);
			System.out.println("Ingresa el numero de filas");
			int fila = sc.nextInt();
			y = Integer.toString(fila);
			if ((columna > 4 && columna < 50) && (fila > 4 && fila < 50)) {
				bien = true;
			} else {
				System.out.println("El rango minimo de filas / columnas son 4  y maximo de 50");
				bien = false;
			}

		}

		sc.nextLine();
		System.out.println("Nombre del jugador 1");
		System.out.println("Si quieres jugar contra el ordenador pon el nombre CPU");
		String jugador1 = sc.nextLine();
		System.out.println("Nombre del jugador 2");
		System.out.println("Si quieres jugar contra el ordenador pon el nombre CPU");
		String jugador2 = sc.nextLine();

		String[] array2 = { x, y, jugador1, jugador2 };
		for (int i = 0; i < array2.length; i++) {
			System.out.print(array2[i] + " ");
		}
		System.out.println("");
		return array2;

	}

	/**
	 * Explica brevemente las reglas del juego
	 */
	private static void ayuda() {
		System.out.println(
				"El objetivo de Conecta 4 es alinear cuatro fichas sobre un tablero formado por seis filas y siete columnas. \r\n"
						+ "Por turnos, los jugadores deben introducir una ficha en la columna que prefieran (siempre que no esté completa) y \r\n"
						+ "ésta caerá a la posición más baja. Gana la partida el primero que consiga alinear cuatro fichas consecutivas en horizontal, vertical o diagonal.​ \r\n"
						+ "Si todas las columnas están llenas pero nadie ha hecho una fila válida, hay empate.\r\n");

	}

}
